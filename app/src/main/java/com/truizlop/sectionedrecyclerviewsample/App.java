package com.truizlop.sectionedrecyclerviewsample;


import ohos.aafwk.ability.AbilityPackage;


public class App extends AbilityPackage {

    private static App instance;

    @Override
    public void onInitialize() {
        super.onInitialize();
        if (instance == null) {
            instance = this;
        }
    }

    public static App getApp() {
        return instance;
    }
}