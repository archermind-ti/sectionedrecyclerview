/*
 * Copyright (C) 2015 Tomás Ruiz-López.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.truizlop.sectionedrecyclerview;


import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

import java.util.List;

public abstract class SimpleSectionedAdapter<VH extends HeaderViewHolder> extends SectionedRecyclerViewAdapter<HeaderViewHolder,
        VH, HeaderViewHolder> {


    public SimpleSectionedAdapter(List<String> listData, Context context) {
        super(listData, context);
    }


    @Override
    protected boolean hasFooterInSection(int section) {
        return false;
    }

    @Override
    protected HeaderViewHolder onCreateSectionHeaderViewHolder(ComponentContainer parent, int viewType) {
        LayoutScatter inflater = LayoutScatter.getInstance(parent.getContext());
        Component view = inflater.parse(getLayoutResource(), parent, false);
        return new HeaderViewHolder(view, getTitleTextID());
    }

    @Override
    protected HeaderViewHolder onCreateSectionFooterViewHolder(ComponentContainer parent, int viewType) {
        return null;
    }

    @Override
    protected void onBindSectionHeaderViewHolder(HeaderViewHolder holder, int section) {
        String title = getSectionHeaderTitle(section);
        holder.render(title);
    }

    @Override
    protected void onBindSectionFooterViewHolder(HeaderViewHolder holder, int section) {
    }


    protected int getLayoutResource() {
        return ResourceTable.Layout_view_header;
    }


    protected int getTitleTextID() {
        return ResourceTable.Id_title_text;
    }


    protected abstract String getSectionHeaderTitle(int section);
}
