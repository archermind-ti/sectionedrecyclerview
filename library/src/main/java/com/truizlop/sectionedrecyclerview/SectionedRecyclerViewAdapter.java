package com.truizlop.sectionedrecyclerview;

import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

public abstract class SectionedRecyclerViewAdapter<H extends HeaderViewHolder, VH extends HeaderViewHolder,
        F extends HeaderViewHolder> extends BaseListProvider<String, SectionedRecyclerViewAdapter.MySectionHolder> {
    private int mColumnCount = 1;
    private final int screenWidth;

    public SectionedRecyclerViewAdapter(List<String> listData, int columnCount, Context context) {
        super(listData, context);
        if (columnCount != 0) {
            mColumnCount = columnCount;
        }
        screenWidth = getScreenWidth(context);
    }

    public SectionedRecyclerViewAdapter(List<String> listData, Context context) {
        super(listData, context);
        screenWidth = getScreenWidth(context);
    }


    @Override
    protected int getItemLayout() {
        return ResourceTable.Layout_item_section;
    }

    @Override
    public void convertItem(int position1, List<String> data, MySectionHolder viewHolder, Component convertComponent1, ComponentContainer componentContainer1) {
        ListContainer listContainer = viewHolder.listContainer;
        boolean b = hasFooterInSection(position1);
        if (b) {
            F f = onCreateSectionFooterViewHolder(componentContainer1, position1);
            onBindSectionFooterViewHolder(f, position1);
            setOtherView(viewHolder.footerId, f.baseComponent);
        } else {
            setOtherView(viewHolder.footerId, null);
        }

        H h = onCreateSectionHeaderViewHolder(componentContainer1, position1);
        onBindSectionHeaderViewHolder(h, position1);
        setOtherView(viewHolder.headerId, h.baseComponent);


        if (mColumnCount > 1) {
            TableLayoutManager tableLayoutManager = new TableLayoutManager();
            tableLayoutManager.setColumnCount(mColumnCount);
            listContainer.setLayoutManager(tableLayoutManager);
        }

        int itemCountForSection = getItemCountForSection(position1);
        SectionItemProvider myItemProvider = new SectionItemProvider(getArryData(itemCountForSection),
                (position, convertComponent, componentContainer) -> {
                    Component cpt;
                    VH viewHolder1;
                    if (convertComponent == null) {
                        viewHolder1 = onCreateItemViewHolder(componentContainer, position);
                        cpt = viewHolder1.baseComponent;
                        cpt.setTag(viewHolder1);
                    } else {
                        cpt = convertComponent;
                        viewHolder1 = (VH) cpt.getTag();
                    }
                    cpt.setWidth(screenWidth / mColumnCount - cpt.getMarginLeft() - cpt.getMarginRight());
                    onBindItemViewHolder(viewHolder1, position1, position);
                    return cpt;
                });
        listContainer.setItemProvider(myItemProvider);
    }


    private void setOtherView(ComponentContainer componentContainer, Component component) {
        if (componentContainer == null) {
            return;
        }
        componentContainer.removeAllComponents();
        if (component != null) {
            componentContainer.addComponent(component);
        }
    }


    @Override
    public MySectionHolder getViewHolder(Component component) {
        return new MySectionHolder(component);
    }


    protected abstract int getItemCountForSection(int section);

    protected abstract boolean hasFooterInSection(int section);

    protected abstract H onCreateSectionHeaderViewHolder(ComponentContainer parent, int viewType);

    protected abstract F onCreateSectionFooterViewHolder(ComponentContainer parent, int viewType);

    protected abstract VH onCreateItemViewHolder(ComponentContainer parent, int viewType);

    protected abstract void onBindSectionHeaderViewHolder(H holder, int section);

    protected abstract void onBindSectionFooterViewHolder(F holder, int section);

    protected abstract void onBindItemViewHolder(VH holder, int section, int position);


    public static List<String> getArryData(int t) {
        ArrayList<String> strings = new ArrayList<>();
        for (int i = 0; i < t; i++) {
            strings.add(i + "");
        }
        return strings;
    }

    public static class MySectionHolder extends BaseViewHolder {

        ListContainer listContainer;
        DirectionalLayout footerId, headerId;

        public MySectionHolder(Component component) {
            super(component);
            listContainer = (ListContainer) component.findComponentById(ResourceTable.Id_myList);
            footerId = (DirectionalLayout) component.findComponentById(ResourceTable.Id_footerId);
            headerId = (DirectionalLayout) component.findComponentById(ResourceTable.Id_headerId);
        }
    }

}
